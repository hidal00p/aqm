# Applied quantum mechanics

This repository contains the program to plot he probability density for this non-stationary state as a function of time.

## Homework 2
The second homework is related to the probability current density, and models for electorn behaviour as gaussian wave packets.

**Time evolution:**
Consider an electron in an infinite potential well with potential V(x) = 0 for x \in [0, L].

Find the solution of the time-dependent Schrödinger equation with the ground state as initial state, i.e., \Psi(x, t = 0) = \phi_{1}(x)
For \Psi(x, t) calculate the probability density |\Psi(x, t)|^{2} and the probability current density J_{x}(x, t)

Find the solution when the initial state is a superposition of eigenfunctions of different energy, \Psi(x, t = 0)= (\phi_{1}(x)+\phi_{2}(x)) / \sqrt{2}
Calculate |\Psi(x, t)|^{2} and the corresponding probability current density J_{x}(x, t). (optionally) Plot the probability density for this non-stationary state as a function of time.