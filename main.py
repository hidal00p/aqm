import numpy as np
import util as u
import matplotlib.pyplot as plt
import argparse as ap

def probability_density(x, t):
    return (1 / u.L) * ((np.sin(u.pi*x/u.L)**2) + (np.sin(2*u.pi*x/u.L)**2) + (u.M(t) * np.sin(u.pi*x/u.L) * np.sin(2*u.pi*x/u.L)))

'''
[x1 - [t1, t2, ...]]
[x2 - [t1, t2, ...]]
[x3 - [t1, t2, ...]]
...
'''
def get_solution():
    x_coor = []
    for x in range(u.x_n + 1):
        t_coor = []
        for t in range(u.t_n + 1):
            t_coor.append(probability_density(u.L * x / (u.x_n), u.T * t / (u.t_n)))
        x_coor.append(t_coor)
    return np.array(x_coor)

def get_stationary(T = 0):
    x_coor = [probability_density(u.L * x / (u.x_n), T) for x in range(u.x_n + 1)]
    return np.array(x_coor)


def plot_stationary(sol, time):
    plt.plot([u.L * x / (u.x_n) for x in range(u.x_n + 1)], sol, label='time ' + str(time))

def plot(sol):
    x_step = u.L / u.x_n
    t_step = u.T / u.t_n
    X, T = [i*x_step for i in range(0, u.x_n+1)], [i*t_step for i in range(0, u.t_n + 1)]
    fig, ax = plt.subplots()
    contf = ax.contourf(T, X, sol)
    
    fig.colorbar(contf, ax=ax)
    ax.set_xlabel('t [s]')
    ax.set_ylabel('x [m]')
    
    plt.show()

def main(type):
    if type == 's':
        for t in [1, 1.125, 1.25, 1.5, 1.75, 1.875, 2, 3, 4]:
            plot_stationary(get_stationary(T = t), t)
        plt.legend()
        plt.show()
    else:
        plot(get_solution())

if __name__ == '__main__':
    parser = ap.ArgumentParser()
    parser.add_argument("--type", type=str, choices=['s', 'h'], help="Plot type", required=True)

    main(
        **vars(parser.parse_args())
    )